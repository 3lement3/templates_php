<form>
    <input type="text" name="x" placeholder="Число">
    <input type="submit">
</form>

<?php

class Number
{
    private $first;

    public function setFirst($first)
    {
        $this->first = $first;
    }

    public function getFirst()
    {
        return $this->first;
    }

    public function filter()
    {
        $options = [
            'options' => [
                'min_range' => -10,
                'max_range' => 10,
            ]
        ];
        if (filter_var($this->getFirst(), FILTER_VALIDATE_INT, $options)) {
            return "Число  " . $this->getFirst() . " In Range between -10 and 10<br/>";
        } else {
            return "Число " . $this->getFirst() . " Not in Range between -10 and 10<br/>";
        }

    }

}

$num = new Number();
$num->setFirst($_GET['x']);
echo $num->filter();