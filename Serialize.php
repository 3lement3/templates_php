<?php
class WORK
{
    const NAME = 'Vasya ';
    const LASTNAME = 'Petrov';

    public function getName()
    {
        echo self::NAME;
    }

    public function getLastName()
    {
        echo self::LASTNAME;
    }
}
$worker = new WORK();
$ser = serialize($worker);
setcookie("name" , $ser, time()+3600);
$a = unserialize($_COOKIE["name"]);
$a->getName();
$a->getLastName();
unset($_COOKIE['name']);