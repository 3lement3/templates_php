<?php
#Templates upload Files and Views
var_dump($_FILES);
/*
Upload_err_ok	значение: 0
файл был успешно загружен на сервер

Upload_err_ini_size	значение: 1
размер принятого файла превысил максимально допустимый размер, который задан директивой upload_max_filesize конфигурационного файла php.ini

Upload_err_form_size	значение: 2
размер загружаемого файла превысил значение max_file_size, указанное в html-форме

Upload_err_partial	значение: 3
загружаемый файл был получен только частично

Upload_err_no_file	значение: 4
файл не был загружен
*/
$nameFile = trim(mb_strtolower($_FILES['file']['name']));
$tmpName = $_FILES['file']['tmp_name'];
if (!file_exists('img')) {
    mkdir('img');
}
$fileName = "img/$nameFile";
move_uploaded_file($tmpName, $fileName);
if ((file_exists($fileName)) && !empty($_FILES)){
    echo 'Upload   ' . $nameFile;
};
?>
<form enctype="multipart/form-data" method="post" action="#">
    <input type="file" name="file">
    <input type="submit" value="submit">
</form>
<?php
#Choose random image format
$fNames = glob("img/*.{jpg,png}", GLOB_BRACE);
#$fname = $fnames[mt_rand(0, count($fnames)-1)];
#To display the image
foreach ($fNames as $image) {
?>
<img src='<?php echo $image ?>' alt='<?php echo $fileName?>' height="200" width="400">
<?php
}
?>
