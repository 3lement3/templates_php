<?php
/**
 * Random  view selection of the brand
 * @author 3lement <djelement@yandex.ru>
 * @version 0.1
 */

/** @var array $planets */
$brend = ["Nokia", "Samsung", "Sony ", "Apple"];

/**
 * @param array {$brend}
 * @return string
 *
 */
function strRand()
{
    $i = mt_rand(0, 3);
    foreach (func_get_args() as $key) {
        echo "Number $i: {$key[$i]}<br>";
    }
}
for ($i = 0; $i != 5; $i++) {
    strRand($brend);
}
