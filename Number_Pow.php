/**
* Create 3lement 6/24/2019
*
*/
<?php
$matPow = 2; // указание возведение в степень
for ($i = 0; $i <= $matPow; $i++) {
    $countTemp = $i; //вспомогательный счетчик
    $result = 1; //стартовой число для возведения
    $power = 5; //число для возведения
    while ($countTemp > 0) {
        $result *= $power;
        $countTemp--;
    }
}
echo "Number {$power}: в степени {$matPow} равно {$result}";