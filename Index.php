<?php
#sort array dot :^);
class Point
{
    public $x;
    public $y;
}

$fst = new Point();
$fst->x = 12;
$fst->y = 5;
$snd = new Point();
$snd->x = 1;
$snd->y = 1;
$thd = new Point();
$thd->x = 4;
$thd->y = 10;
$arr = [$fst , $snd , $thd];
var_dump($arr); //->original array
echo '<hr>';
usort($arr, function ($a, $b) {
   $a = sqrt($a->x ** 2 + $a->y ** 2);
   $b = sqrt($b->x ** 2 + $b->y ** 2);
   return $a <=> $b;
});
var_dump($arr); //-> The result array


##########################################
#sessin_start )^;
session_start();

if ($_POST['name']) {
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['id'] = $_POST['id'];
} else {
    unset($_SESSION);
}
if ($_SESSION['name']) {
    echo $_SESSION['name'];
}
var_dump($_SESSION);
?>

<form action="#" method="post">
    <input type="text" name="name">
    <input type="hidden" name="id" value="5">
    <input type="submit" value="enter">
</form>

