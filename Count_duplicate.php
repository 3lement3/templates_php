<?php
/**@author 3lement 19.06.2019
 *Подсчет количества полученных одинаковых чисел при использование *mt_rand()
/**
 * @return int Генерация числа
 */
function randomNumber()
{
    return mt_rand(0, 10);
}
$i = 0;
while ($i < 100) {
    $arr[] = randomNumber();
    $i++;
}
/** @var Подсчитывает количество всех значений массива $countArray */
$countArray = array_count_values($arr);
/**
 *Сортируем массив по ключам
 */
ksort($countArray) ;
/**
 * Перебираем массив с выводом числа и количеством его повторения
 */
foreach ($countArray as $number => $count) {
    echo "Массив созданный на основе mt_rand число: <b>{$number}</b>" . " повторяется" . " <b>{$count}</b> раз" . '<br>';
}